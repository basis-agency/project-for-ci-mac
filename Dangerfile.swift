//
//  AppDelegate.swift
//  NFC Scan MKB
//
//  Created by Sergey Melnik on 18.04.2023.
//

let editedFiles = danger.git.modifiedFiles + danger.git.createdFiles
message("These files have changed: \(editedFiles.joined(separator: ", "))")
