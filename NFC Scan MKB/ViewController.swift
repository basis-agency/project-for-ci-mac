//
//  ViewController.swift
//  NFC Scan MKB
//
//  Created by Sergey Melnik on 18.04.2023.
//

import UIKit
import CoreNFC
import SwiftyJSON

class ViewController: UIViewController, NFCTagReaderSessionDelegate {
    @IBOutlet weak var NFCText: UITextView!
    var nfcTagReaderSession: NFCTagReaderSession?
    var nfcSession: NFCTagReaderSession?
    var word = "none"
    @IBAction func scanBtn(_ sender: Any) {
        guard NFCTagReaderSession.readingAvailable else {
            let alertController = UIAlertController(
                title: "Скнирование не поддерживается",
                message: "Устройство не поддерживает сканирование Вашей карты.",
                preferredStyle: .alert
            )
            alertController.addAction(UIAlertAction(title: "ОК", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
            return
        }
        nfcTagReaderSession = NFCTagReaderSession(pollingOption: [.iso14443], delegate: self)
        nfcTagReaderSession?.begin()
        print("isReady: \(String(describing: nfcTagReaderSession?.isReady))")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        NFCText.textColor = .green
        NFCText.text = ""
        NFCText.isEditable = false
    }
    func connect(
        to tag: NFCTag,
        completionHandler: @escaping ((Error)?) -> Void
    ) {}
    func tagReaderSessionDidBecomeActive(_ session: NFCTagReaderSession) {
        print("Сессия start")
    }
    func tagReaderSession(_ session: NFCTagReaderSession, didInvalidateWithError error: Error) {
        print("Сессия упала из-за: \(error.localizedDescription)")
    }
    func tagReaderSession(_ session: NFCTagReaderSession, didDetect tags: [NFCTag]) {
        print("Массив Тегов = \(tags)")
        
        let tag = tags.first!
        
        nfcTagReaderSession?.connect(to: tag) { (error: Error?) in
            if case let .iso7816(iso7816Tag) = tag {
                
                print("ID hash value: \(iso7816Tag.initialSelectedAID.description)")
                print("ID base64EncodedString: \(iso7816Tag.identifier.customMirror.description)")
                print("ID base64EncodedData: \(iso7816Tag.identifier.base64EncodedData())")
                print("ID historicalBytes: \(String(describing: iso7816Tag.historicalBytes?.base64EncodedString()))")
                print("ID applicationData: \(String(describing: iso7816Tag.applicationData?.base64EncodedString()))")
                print("ID DataCoding: \(String(describing: iso7816Tag.proprietaryApplicationDataCoding))")
                //                do {
                //                    let json = try JSONSerialization.jsonObject(with: iso7816Tag.identifier.base64EncodedData(), options: []) as? [String : Any]
                //                    print("base64EncodedString: \(String(describing: json))")
                //                } catch {
                //                    print("JSONSerialization error:", error)
                //                }
                iso7816Tag.sendCommand(apdu: .init(instructionClass: .min,
                                                   instructionCode: .min,
                                                   p1Parameter: .min,
                                                   p2Parameter: .min,
                                                   data: Data(),
                                                   expectedResponseLength: 100),
                                                completionHandler: { data,_,_,_ in
                    print("sendCommand \(data.base64EncodedString())")
                })
                //                tagData.append("\(iso7816Tag.proprietaryApplicationDataCoding)\n")
                //                print("hash: \(iso7816Tag.hash)")
                //                tagData.append("\(iso7816Tag.hash)\n")
                //                print("is avalible: \(iso7816Tag.isAvailable)")
                //                tagData.append("\(iso7816Tag.isAvailable)\n")
                //                print("is avalible: \(iso7816Tag.applicationData)")
                self.NFCText.text = iso7816Tag.applicationData?.base64EncodedString()
            }
        }
        nfcTagReaderSession?.restartPolling()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
